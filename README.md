# Build docker images for proofgeneral/coq-nix

[![tags](https://img.shields.io/badge/tags%20on-docker%20hub-blue.svg)](https://hub.docker.com/r/proofgeneral/coq-nix#supported-tags "Supported tags on Docker Hub")
[![pipeline status](https://gitlab.com/hendriktews/coq-nix-docker/badges/master/pipeline.svg)](https://gitlab.com/hendriktews/coq-nix-docker/-/pipelines)
[![pulls](https://img.shields.io/docker/pulls/proofgeneral/coq-nix.svg)](https://hub.docker.com/r/proofgeneral/coq-nix "Number of pulls from Docker Hub")
[![stars](https://img.shields.io/docker/stars/proofgeneral/coq-nix.svg)](https://hub.docker.com/r/proofgeneral/coq-nix "Star the image on Docker Hub")  
[![dockerfile](https://img.shields.io/badge/dockerfile%20on-gitlab-blue.svg)](https://gitlab.com/hendriktews/coq-nix-docker "Dockerfile source repository")
[![coq](https://img.shields.io/badge/depends%20on-coqorg%2Fcoq-blue.svg)](https://hub.docker.com/r/coqorg/coq "Docker images of Coq")

This repository provides [Docker](https://www.docker.com/) images
with [nix](https://nixos.org/) installed, providing the basis for
[coq-emacs-docker](https://gitlab.com/hendriktews/coq-emacs-docker/).

These images are based on the
[coqorg/coq](https://hub.docker.com/r/coqorg/coq/) images, itself
based on [Debian 10 Slim](https://hub.docker.com/_/debian/).

<!--
 !-- See also the [docker-coq wiki](https://github.com/coq-community/docker-coq/wiki) for details about how to use such images locally or in a CI context.
  -->

<!-- tags -->

This repository incorporates the
[docker-keeper](https://gitlab.com/erikmd/docker-keeper) tool as a
[subtree](https://www.atlassian.com/git/tutorials/git-subtree).

For more details, see the documentation available in the
[docker-keeper wiki](https://gitlab.com/erikmd/docker-keeper/-/wikis/home).
